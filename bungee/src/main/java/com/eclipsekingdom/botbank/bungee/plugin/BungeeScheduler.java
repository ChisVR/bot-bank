package com.eclipsekingdom.botbank.bungee.plugin;

import com.eclipsekingdom.botbank.bungee.BungeeMain;
import com.eclipsekingdom.botbank.util.plugin.IScheduler;
import net.md_5.bungee.api.scheduler.TaskScheduler;

public class BungeeScheduler implements IScheduler {

    private BungeeMain plugin = BungeeMain.getPlugin();
    private TaskScheduler scheduler = plugin.getProxy().getScheduler();

    @Override
    public void runAsync(Runnable r) {
        scheduler.runAsync(plugin, r);
    }
}
