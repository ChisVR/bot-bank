package com.eclipsekingdom.botbank.bungee.plugin;

import com.eclipsekingdom.botbank.bungee.BungeeMain;
import com.eclipsekingdom.botbank.util.plugin.IConsole;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;

public class BungeeConsole implements IConsole {

    private CommandSender commandSender = BungeeMain.getPlugin().getProxy().getConsole();

    @Override
    public void raw(String s) {
        commandSender.sendMessage(new TextComponent(s));
    }

    @Override
    public void info(String s) {
        raw("[BotBank] " + s);
    }

    @Override
    public void warn(String s) {
        raw("[BotBank] [WARN]" + s);
    }
}
