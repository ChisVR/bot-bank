package com.eclipsekingdom.botbank.bungee.plugin;

import com.eclipsekingdom.botbank.util.plugin.FileConfig;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.util.Collection;
import java.util.List;

public class BungeeConfig implements FileConfig {

    private Configuration config;

    public BungeeConfig(Configuration config) {
        this.config = config;
    }

    @Override
    public boolean contains(String path) {
        return config.contains(path);
    }

    @Override
    public Collection<String> getSection(String path) {
        return config.getSection(path).getKeys();
    }

    @Override
    public String getString(String path, String defaultValue) {
        return config.getString(path, defaultValue);
    }

    @Override
    public int getInt(String path, int defaultValue) {
        return config.getInt(path, defaultValue);
    }

    @Override
    public boolean getBoolean(String path, boolean defaultValue) {
        return config.getBoolean(path, defaultValue);
    }
}
