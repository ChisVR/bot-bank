package com.eclipsekingdom.botbank.bungee;

import com.eclipsekingdom.botbank.BotBankListener;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.event.EventHandler;

public class BungeeListener implements Listener {


    public BungeeListener() {
        Plugin plugin = BungeeMain.getPlugin();
        plugin.getProxy().getPluginManager().registerListener(plugin, this);
    }

    @EventHandler
    public void onPostLogin(PostLoginEvent e) {
        BotBankListener.onJoin();
    }

    @EventHandler
    public void onQuit(PlayerDisconnectEvent e) {
        BotBankListener.onQuit();
    }

}
