package com.eclipsekingdom.botbank.api;

import com.eclipsekingdom.botbank.bot.Bot;
import com.eclipsekingdom.botbank.bot.BotCache;
import com.eclipsekingdom.botbank.command.IBotCommandExecutor;
import net.dv8tion.jda.api.JDA;

public class LaunchedBot {

    protected Bot bot;
    protected DiscordApp discordApp;

    public LaunchedBot(Bot bot, DiscordApp discordApp) {
        this.bot = bot;
        this.discordApp = discordApp;
    }

    public JDA getJDA() {
        return bot.getJDA();
    }

    public boolean isOnline() {
        return bot.getStatus().isOnline();
    }

    public void registerCommand(String root, BotCommandExecutor executor) {
        BotCache.registerBotCommand(bot, discordApp.getNamespace(), root, asIOrderExecutor(executor));
    }

    public void unregisterCommand(String root) {
        BotCache.unregisterBotCommand(bot.getId(), discordApp.getNamespace(), root);
    }

    public String getCommandPrefix() {
        return bot.getCommandPrefix();
    }

    protected IBotCommandExecutor asIOrderExecutor(BotCommandExecutor botCommandExecutor) {
        return (sender, guild, channel, message, args) -> botCommandExecutor.onCommand(sender, guild, channel, message, args);
    }

}
