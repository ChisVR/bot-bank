package com.eclipsekingdom.botbank.api;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;

public interface BotCommandExecutor {

    void onCommand(Member sender, Guild guild, TextChannel channel, Message message, String[] args);

}
