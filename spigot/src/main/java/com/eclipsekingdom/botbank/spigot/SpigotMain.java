package com.eclipsekingdom.botbank.spigot;

import com.eclipsekingdom.botbank.BotBank;
import com.eclipsekingdom.botbank.BotBankListener;
import com.eclipsekingdom.botbank.config.PluginConfig;
import com.eclipsekingdom.botbank.spigot.plugin.SpigotPlugin;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public final class SpigotMain extends JavaPlugin {

    private static SpigotMain plugin;

    @Override
    public void onEnable() {
        plugin = this;
        BotBank.startup(new SpigotPlugin());
        new SpigotListener();
        Bukkit.getScheduler().runTaskLaterAsynchronously(this, () -> {
            BotBankListener.onLoad();
        }, PluginConfig.getAppRegistrationPeriod() * 20);
    }

    @Override
    public void onDisable() {
        BotBank.shutdown();
    }

    public static SpigotMain getPlugin() {
        return plugin;
    }

}
