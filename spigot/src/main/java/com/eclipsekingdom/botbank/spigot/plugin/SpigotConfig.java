package com.eclipsekingdom.botbank.spigot.plugin;

import com.eclipsekingdom.botbank.util.plugin.FileConfig;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.Collection;
import java.util.Collections;

public class SpigotConfig implements FileConfig {

    private FileConfiguration config;

    public SpigotConfig(FileConfiguration config) {
        this.config = config;
    }

    @Override
    public boolean contains(String path) {
        return config.contains(path);
    }

    @Override
    public Collection<String> getSection(String path) {
        if (config.contains(path)) {
            return config.getConfigurationSection(path).getKeys(false);
        } else {
            return Collections.EMPTY_LIST;
        }
    }

    @Override
    public String getString(String path, String defaultValue) {
        return config.getString(path, defaultValue);
    }

    @Override
    public int getInt(String path, int defaultValue) {
        return config.getInt(path, defaultValue);
    }

    @Override
    public boolean getBoolean(String path, boolean defaultValue) {
        return config.getBoolean(path, defaultValue);
    }

}
