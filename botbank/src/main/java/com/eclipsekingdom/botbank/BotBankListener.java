package com.eclipsekingdom.botbank;

import com.eclipsekingdom.botbank.bot.Launcher;
import com.eclipsekingdom.botbank.server.Server;

public class BotBankListener {

    public static void onLoad() {
        Launcher.launch();
    }

    public static void onJoin() {
        Server.onJoin();
    }

    public static void onQuit() {
        Server.onQuit();
    }

}
