package com.eclipsekingdom.botbank.util.plugin;

public interface IPlugin {

    String getVersion();

    ConfigLoader getConfigLoader();

    IScheduler getScheduler();

    IConsole getConsole();

    int getOnlinePlayerCount();

}
