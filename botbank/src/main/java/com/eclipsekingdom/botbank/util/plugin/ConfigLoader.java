package com.eclipsekingdom.botbank.util.plugin;

import java.io.File;

public interface ConfigLoader {

    FileConfig load(File file);

}
