package com.eclipsekingdom.botbank.command;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;

public interface IBotCommandExecutor {

    void onOrder(Member sender, Guild guild, TextChannel channel, Message message, String[] args);

}
