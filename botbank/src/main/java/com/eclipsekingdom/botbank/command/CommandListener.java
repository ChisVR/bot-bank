package com.eclipsekingdom.botbank.command;

import com.eclipsekingdom.botbank.bot.Bot;
import com.eclipsekingdom.botbank.bot.BotCache;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.util.UUID;

public class CommandListener extends ListenerAdapter {

    private Bot bot;
    private UUID botId;

    public CommandListener(Bot bot) {
        this.bot = bot;
        this.botId = bot.getId();
    }

    public void register() {
        bot.getJDA().addEventListener(this);
    }

    public void unregister() {
        bot.getJDA().removeEventListener(this);
    }

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent e) {
        User user = e.getAuthor();
        if (user.isBot() || user.isFake()) return;
        String prefix = bot.getCommandPrefix();
        String raw = e.getMessage().getContentRaw();
        if (raw.startsWith(prefix)) {
            if (raw.length() > prefix.length()) {
                String afterPrefix = raw.substring(prefix.length());
                String[] args = afterPrefix.split(" ");
                String root = args[0];
                IBotCommandExecutor executor = BotCache.getExecutor(botId, root);
                if (executor != null) {
                    String[] shiftedArgs = new String[args.length - 1];
                    for (int i = 1; i < args.length; i++) {
                        shiftedArgs[i - 1] = args[i];
                    }
                    Member sender = e.getMember();
                    Guild guild = e.getGuild();
                    TextChannel channel = e.getChannel();
                    executor.onOrder(sender, guild, channel, e.getMessage(), shiftedArgs);
                }
            }
        }

        super.onGuildMessageReceived(e);
    }


}
