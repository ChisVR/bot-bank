package com.eclipsekingdom.botbank.bot;

import net.dv8tion.jda.api.entities.Activity;

public class BotPresence {

    private boolean enabled;
    private Activity.ActivityType type;
    private String message;

    public BotPresence(boolean enabled, Activity.ActivityType type, String message) {
        this.enabled = message != null && enabled;
        this.type = type;
        this.message = message;
    }

    public boolean isEnabled() {
        return enabled && type != null && message != null;
    }

    public Activity.ActivityType getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    public boolean hasOnlinePlaceholder() {
        return message != null && message.contains("%online%");
    }

}
