package com.eclipsekingdom.botbank.config;

import com.eclipsekingdom.botbank.bot.Bot;
import com.eclipsekingdom.botbank.bot.BotPresence;
import com.eclipsekingdom.botbank.util.plugin.Console;
import com.eclipsekingdom.botbank.util.plugin.FileConfig;
import com.eclipsekingdom.botbank.util.plugin.Plugin;
import net.dv8tion.jda.api.entities.Activity;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class BotsConfig {

    private static final String BOTS_FIELD = "bots";
    private static List<Bot> bots = new ArrayList<>();

    public static void load() {
        File file = new File("plugins/BotBank", "bots.yml");
        if (file.exists()) {
            FileConfig config = Plugin.getConfigLoader().load(file);
            try {
                List<Bot> loadedBots = new ArrayList<>();
                for (String botName : config.getSection(BOTS_FIELD)) {
                    String key = BOTS_FIELD + "." + botName;
                    String token = config.getString(key + ".token", "0");

                    String activityKey = key + ".activity";
                    boolean enabled = true;
                    if (config.contains(activityKey + ".enable")) {
                        enabled = config.getBoolean(activityKey + ".enable", true);
                    }

                    Activity.ActivityType activityType = Activity.ActivityType.DEFAULT;
                    if (config.contains(activityKey + ".type")) {
                        activityType = getType(config.getString(activityKey + ".type", ""));
                    }

                    String activityMessage = "Minecraft";
                    if (config.contains(activityKey + ".message")) {
                        activityMessage = config.getString(activityKey + ".message", "Minecraft");
                    }

                    BotPresence presence = new BotPresence(enabled, activityType, activityMessage);

                    String commandPrefix = "!";
                    if (config.contains(key + ".command prefix")) {
                        commandPrefix = config.getString(key + ".command prefix", "!");
                    }
                    loadedBots.add(new Bot(botName, token, commandPrefix, presence));
                }
                bots.addAll(loadedBots);
                loadedBots.clear();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static Activity.ActivityType getType(String s) {
        try {
            return Activity.ActivityType.valueOf(s);
        } catch (Exception e) {
            return null;
        }
    }

    public static List<Bot> getBots() {
        return bots;
    }

}
